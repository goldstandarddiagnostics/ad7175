//
// Created by dnatov on 9/27/2021.
//

#include "AD7175_2.h"

int8_t AD7175_ReadRegister8(struct AD7175_t* instance, uint8_t reg)
{
	uint8_t buffer[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	/* Build the Command word */
	buffer[0] = COMM_REG_WEN | COMM_REG_RD | reg;

	/* Read data from the device */
	instance->SPI_ReadWriteMethodPtr(&buffer[0], 2, instance->ChipSelectID, spmMode3);

	return buffer[1];
}

int16_t AD7175_ReadRegister16(struct AD7175_t* instance, uint8_t reg)
{
	int16_t ret       = 0;
	uint8_t buffer[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	/* Build the Command word */
	buffer[0] = COMM_REG_WEN | COMM_REG_RD | reg;

	/* Read data from the device */
	instance->SPI_ReadWriteMethodPtr(&buffer[0], 3, instance->ChipSelectID, spmMode3);

	/* Build the result */
	for(int i = 1; i < 3; i++)
	{
		ret <<= 8;
		ret += buffer[i];
	}

	return ret;
}

int32_t AD7175_ReadRegister24(struct AD7175_t* instance, uint8_t reg)
{
	int32_t ret       = 0;
	uint8_t buffer[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	/* Build the Command word */
	buffer[0] = COMM_REG_WEN | COMM_REG_RD | reg;

	/* Read data from the device */
	instance->SPI_ReadWriteMethodPtr(&buffer[0], 4, instance->ChipSelectID, spmMode3);

	/* Build the result */
	for(int i = 1; i < 4; i++)
	{
		ret <<= 8;
		ret += buffer[i];
	}

	return ret;
}

void AD7175_WriteRegister8(struct AD7175_t* instance, uint8_t reg, uint8_t value)
{
	uint8_t wrBuf[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	/* Build the Command word */
	wrBuf[0] = COMM_REG_WEN | COMM_REG_WR | reg;

	/* Fill the write buffer */
	wrBuf[1] = value;

	/* Write data to the device */
	instance->SPI_ReadWriteMethodPtr(&wrBuf[0], 2, instance->ChipSelectID, spmMode3);
}

void AD7175_WriteRegister16(struct AD7175_t* instance, uint8_t reg, uint16_t value)
{
	uint8_t wrBuf[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	/* Build the Command word */
	wrBuf[0] = COMM_REG_WEN | COMM_REG_WR | reg;

	/* Fill the write buffer */
	for(int i = 0; i < 2; i++)
	{
		wrBuf[2 - i] = value & 0xFF;
		value >>= 8;
	}

	/* Write data to the device */
	instance->SPI_ReadWriteMethodPtr(&wrBuf[0], 3, instance->ChipSelectID, spmMode3);
}

void AD7175_WriteRegister24(struct AD7175_t* instance, uint8_t reg, uint32_t value)
{
	uint8_t wrBuf[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	/* Build the Command word */
	wrBuf[0] = COMM_REG_WEN | COMM_REG_WR | reg;

	/* Fill the write buffer */
	for(int i = 0; i < 3; i++)
	{
		wrBuf[3 - i] = value & 0xFF;
		value >>= 8;
	}

	/* Write data to the device */
	instance->SPI_ReadWriteMethodPtr(&wrBuf[0], 4, instance->ChipSelectID, spmMode3);
}

void AD7175_Init(struct AD7175_t* instance, uint8_t Cs)
{
    instance->ChipSelectID = Cs;
    instance->AD7175_ConfigurationMethodPtr();
	instance->LastUsedChannel = 255; //set to a non-existing channel
}

uint8_t GetChannelConfigAddress(uint8_t aChannelIndex)
{
	switch (aChannelIndex)
	{
	case 0:
		return AD7175_CH0;
		break;
	case 1:
		return AD7175_CH1;
		break;
	case 2:
		return AD7175_CH2;
		break;
	case 3:
		return AD7175_CH3;
		break;
	default:
		return AD7175_CH0;
	}
}

uint32_t AD7175_GetADCValue(struct AD7175_t* instance, uint8_t channelIndex)
{
	if(instance->LastUsedChannel != channelIndex) //change channel
	{
	  if(instance->LastUsedChannel < 4) // turn off old channel
	  {
		  uint8_t regAddress = GetChannelConfigAddress(instance->LastUsedChannel);
		  uint16_t prevValue = AD7175_ReadRegister16(instance, regAddress);
		  prevValue &= ~CH_MAP_REG_CHEN;
		  AD7175_WriteRegister16(instance, regAddress, prevValue);
	  }
	  uint8_t regAddress = GetChannelConfigAddress(channelIndex);
	  uint16_t prevValue = AD7175_ReadRegister16(instance, regAddress);
	  prevValue |= CH_MAP_REG_CHEN;
	  AD7175_WriteRegister16(instance, regAddress, prevValue);
	  instance->LastUsedChannel = channelIndex;
	}

	//start ADC
	uint16_t mode = AD7175_ReadRegister16(instance, AD7175_ADCMODE) & 0b1110011100001100;
	mode |=  ADC_MODE_REG_MODE_SINGLE;
	AD7175_WriteRegister16(instance, AD7175_ADCMODE, mode);

    //wait until ready
	bool ready = false;
	while(!ready)
	{
		/* Read the value of the Status Register */
		uint8_t stat = AD7175_ReadRegister8(instance, AD7175_STATUS);
		/* Check the RDY bit in the Status Register */
		ready = ((stat & STATUS_REG_RDY) == 0);
	}

	//read ADC data
	return AD7175_ReadRegister24(instance, AD7175_DATA);
}

bool AD7175_ReadGPIO(struct AD7175_t* instance, uint8_t gpioPin)
{
    uint16_t gpioReg = AD7175_ReadRegister16(instance, AD7175_GPIOCON);

    if (gpioPin == 0)
    {
        return gpioReg & 0x1;
    }
    else
    {
        return gpioReg & 0x2;
    }
}

void AD7175_SetGPIO(struct AD7175_t* instance, uint8_t gpioPin)
{
    uint16_t gpioReg = AD7175_ReadRegister16(instance, AD7175_GPIOCON);
    if (gpioPin == 0)
    {
        uint16_t newGpioReg = (uint16_t)gpioReg | 0x1;
        AD7175_WriteRegister16(instance, AD7175_GPIOCON, newGpioReg);
    }
    else
    {
        uint16_t newGpioReg = (uint16_t)gpioReg | 0x2;
        AD7175_WriteRegister16(instance, AD7175_GPIOCON, newGpioReg);
    }
}

void AD7175_ResetGPIO(struct AD7175_t* instance, uint8_t gpioPin)
{
    uint16_t gpioReg = AD7175_ReadRegister16(instance, AD7175_GPIOCON);
    if (gpioPin == 0)
    {
        uint16_t newGpioReg = (uint16_t)gpioReg & ~(0x1);
        AD7175_WriteRegister16(instance, AD7175_GPIOCON, newGpioReg);
    }
    else
    {
        uint16_t newGpioReg = (uint16_t)gpioReg & ~(0x2);
        AD7175_WriteRegister16(instance, AD7175_GPIOCON, newGpioReg);
    }
}